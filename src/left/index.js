import React from 'react';
import IconList from '../common/components/IconList'

const Left = () => {
    return (
        <div className="column col-sm-2 hidden-xs">
            <div className="full pull-right" style={{paddingTop: 86+'px'}}>
                <ul className="nav">
                    <IconList 
                        className="active" 
                        iconClassName="glyphicon glyphicon-home"
                        url="/home"
                        text="Home" />

                    <IconList 
                        className="active" 
                        iconClassName="glyphicon glyphicon-list-alt"
                        url="/featured"
                        text="Featured" />

                     <IconList                         
                        iconClassName="glyphicon glyphicon-list"
                        url="stories"
                        text="Stories" />
                  
                    <IconList                         
                        iconClassName="glyphicon glyphicon-refresh"
                        url="Refresh"
                        text="Refresh" />
                </ul>										
            </div>
        </div>
    )
}

export default Left;
