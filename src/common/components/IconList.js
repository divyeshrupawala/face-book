import React from 'react'

const IconList = ({className, iconClassName, url, text}) => {
    return (
        <React.Fragment>
            <li className={className}>
                <a href={url}><i className={iconClassName}></i> {text}</a>
            </li>
        </React.Fragment>
    )
}

export default IconList;