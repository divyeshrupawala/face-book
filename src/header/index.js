import React from 'react';

const Header = () => {
    return (
        <React.Fragment>
            <div className="navbar navbar-blue navbar-static-top">  
                <div className="navbar-header">
                    <button className="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                        <span className="sr-only">Toggle</span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                    </button>
                    <a href="http://usebootstrap.com/theme/facebook" className="navbar-brand logo">f</a>
                </div>

                <nav className="collapse navbar-collapse" role="navigation">
                    <form className="navbar-form navbar-left">
                        <div className="input-group input-group-sm" style={{maxWidth:360+'px'}}>
                            <input className="form-control" placeholder="Search" name="srch-term" id="srch-term" type="text"/>
                            <div className="input-group-btn">
                                <button className="btn btn-default" type="submit">
                                    <i className="glyphicon glyphicon-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>

                    <ul className="nav navbar-nav pull-right">                       
                        <li>
                            <a href="/home">Home</a>
                        </li>
                        <li>
                            <a href="/featured">Featured</a>
                        </li>                        
                        <li>
                            <a href="#"><i className="glyphicon glyphicon-cog"></i></a>
                        </li>
                    </ul>

                    <ul className="nav navbar-nav visible-xs">
                        <li className="active">
                            <a href="/home"><i className="glyphicon glyphicon-list-alt"></i> Home</a>
                        </li>
                        <li>
                            <a href="/featured"><i className="glyphicon glyphicon-list"></i> Featured</a>
                        </li>
                    </ul>	
                </nav>
            </div>
      </React.Fragment>
    )
};

export default Header;