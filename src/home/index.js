import React from 'react'

import { compose } from 'redux'

import WhatsNew from './components/WhatsNew'
import withInfiniteScroll from './components/WithInfiniteScroll'
import List from './components/List'
import Right from './components/Right'

const Center = () => {
    const obj = {
        hits : [{
                  objectID : 1
                },
                {
                  objectID : 2
                },
                {
                  objectID : 3
                },
                {
                  objectID : 4
                },
                {
                  objectID : 5
                }],
        page : 5
      }

    return (
        <React.Fragment>
            <div className="col-sm-9">
                <WhatsNew />
                <ListWithLoadingWithInfinite
                    list={obj.hits}
                    page={obj.page} />              
            </div>
            <Right />
        </React.Fragment>
    )
}

const ListWithLoadingWithInfinite = compose(withInfiniteScroll)(List);

export default Center;