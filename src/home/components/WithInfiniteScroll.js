import React from 'react'

const WithInfiniteScroll = (Component) =>
  class WithInfiniteScroll extends React.Component {
    constructor(props) {
      super(props);
      this.state = { postList : [...props.list] }
      this.scrollPanelEl = document.getElementById('scrol-panel');
    }

    getScrollPanel() {
      return this.scrollPanelEl ? this.scrollPanelEl : document.getElementById('scrol-panel');
    }

    debounce (func, delay = 500) {
      let timeout
  
      return function () {
          clearTimeout(timeout)
          timeout = setTimeout(() => { func.apply(this, arguments) }, delay)
      }
    }

    componentDidMount() {
      this.getScrollPanel().addEventListener('scroll', this.debounce(this.onScroll, 200), false);     
    }

    componentWillUnmount() {
      this.getScrollPanel().removeEventListener('scroll', this.onScroll, false);
    }

    onScroll = () => {
      const scrollPanel = this.getScrollPanel();   
      const currentScrollPosition = scrollPanel.offsetHeight + scrollPanel.scrollTop;
      const scrollHeight = scrollPanel.scrollHeight - 1000;

      if (currentScrollPosition >= scrollHeight) {
          this.setState((state, props) => ({
            postList: [...state.postList,...props.list]
          }));
          scrollPanel.scrollTop = scrollHeight;        
      }
    }

    render() {      
      return <Component postList={this.state.postList} />;
    }
  }

  export default WithInfiniteScroll;