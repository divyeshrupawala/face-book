import React, { Component } from 'react'
import Post from './Post'

const List = ({ postList }) => {    
    return (
        <div className="list">
            {postList.map(item => <Post />)}
        </div>
    )
}

export default List;