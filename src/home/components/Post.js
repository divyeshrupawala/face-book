import React from 'react'
import Circle from '../../assets/img/150x150.gif'
import Bg from '../../assets/img/bg_4.jpg'

const Post = () => {
    return (
        <div className="panel panel-default">											
            <div className="panel-body">
                <img 
                    src={Circle} 
                    style={{width: 40+'px',height: 40+'px',margin: '0px 10px 10px 0px'}} 
                    className="img-circle pull-left"/> 
                <a href="#">Divyesh Rupawala</a>
                <div className="clearfix"></div>												 
            
                <p>This is post text in react using component driven development</p>
                <div className="panel-thumbnail">
                    <img src={Bg} className="img-responsive" style={{maxHeight: 260+'px',width: '100%'}} />
                </div>
                <hr/>
                <form>
                    <div className="input-group">
                        <div className="input-group-btn">
                            <button className="btn btn-default like-share-comment"><i className="glyphicon glyphicon-thumbs-up"></i> Like</button>
                            <button className="btn btn-default like-share-comment"><i className="glyphicon glyphicon-comment"></i> Comment</button>
                            <button className="btn btn-default like-share-comment"><i className="glyphicon glyphicon-share"></i> Share</button>
                        </div>                            
                    </div>
                </form>
                <hr/>
                <input className="form-control" placeholder="Add a comment.." type="text"/>
            </div>
        </div>
    )
}

export default Post;