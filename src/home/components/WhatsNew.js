import React from 'react'

const WhatsNew = () => {
    return (
        <React.Fragment>
            <div className="well"> 
                <form className="form-horizontal" role="form">
                    <h4>What's New</h4>
                    <div className="form-group" style={{padding:14+'px'}}>
                        <textarea className="form-control" placeholder="Update your status"></textarea>
                    </div>
                    <button className="btn btn-primary pull-right" type="button">Post</button>
                    <ul className="list-inline">
                        <li>
                            <a href=""><i className="glyphicon glyphicon-upload"></i></a>
                        </li>                        
                        <li>
                            <a href=""><i className="glyphicon glyphicon-map-marker"></i></a>
                        </li>
                    </ul>
                </form>
            </div>
        </React.Fragment>
    )
}

export default WhatsNew;