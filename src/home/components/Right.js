import React from 'react'

import Bg from '../../assets/img/bg_5.jpg'
import User from '../../assets/img/uFp_tsTJboUY7kue5XAsGAs28.png'

const Right = () => {
    return (
        <div className="col-sm-3 hidden-xs">                    
            <div className="panel panel-default">
                <div className="panel-thumbnail">
                    <img src={Bg} className="img-responsive" />
                </div>
                <div className="panel-body">
                    <p className="lead">New York City</p>
                    <p>45 Followers, 13 Posts</p>
                    
                    <p>
                        <img src={User} height="28px" width="28px"/>
                    </p>
                </div>
            </div>
        </div>
    )
}

export default Right;