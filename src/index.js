import React from 'react';
import ReactDOM from 'react-dom';

import RouterHandler from './RouterHandler'
import registerServiceWorker from './registerServiceWorker';

import './assets/css/bootstrap.css'
import './assets/css/facebook.css'

ReactDOM.render(<RouterHandler />, document.getElementById('root'));
registerServiceWorker();
