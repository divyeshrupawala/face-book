import React from 'react'

const Footer = () => {
    return (
        <div className="row" id="footer">    
            <div className="col-sm-6"></div>
            <div className="col-sm-6">
                <a href="#" className="pull-right">@ Copyright 2013</a>                      
            </div>
        </div>
    )
}

export default Footer;