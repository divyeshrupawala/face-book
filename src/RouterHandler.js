import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import Header from './header'
import Left from './left'
import Home from './home'
import Footer from './footer'
import Featured from './featured'

const RouterHandler = () => {
  return (
    <BrowserRouter>
      <div className="wrapper">
        <div className="box">
          <div className="row row-offcanvas row-offcanvas-left">
            <div className="column col-sm-12 col-xs-12" id="main">
              <Header />     
              <Left/>

              <div id="scrol-panel" className="padding column col-sm-10 col-xs-12">
                <div className="full col-sm-12">	
                  <div className="row">
                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route path="/home" component={Home} />
                        <Route path="/featured" component={Featured} />                        
                    </Switch>                 
                  </div>            
                  <Footer />                  
                </div>
              </div>              
            </div>
          </div>
        </div>
      </div>	
    </BrowserRouter>
  );
};


export default RouterHandler;
